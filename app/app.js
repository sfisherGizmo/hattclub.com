var app = angular.module('hattClub', ['ngRoute', 'ngAnimate', 'toaster']);

var appControllers = angular.module('appControllers', []);

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
            when('/login', {
                title: 'Login',
                templateUrl: 'partials/login.html',
                controller: 'AuthController'
            })
            .when('/logout', {
                title: 'Logout',
                templateUrl: 'partials/login.html',
                controller: 'LogoutController'
            })
            .when('/signup', {
                title: 'Signup',
                templateUrl: 'partials/signup.html',
                controller: 'AuthController'
            })
            .when('/dashboard', {
                title: 'Dashboard',
                templateUrl: 'partials/dashboard.html',
                controller: 'AuthController'
            })
            .when('/', {
                title: 'Login',
                templateUrl: 'partials/login.html',
                controller: 'AuthController',
                role: '0'
            })
            .when('/user/:uid', {
                title: 'Profile',
                templateUrl: 'partials/user.html',
                controller: 'UserController',
            })
            .otherwise({
                redirectTo: '/login'
            });
    }])
    .run(function ($rootScope, $location, Data ) {
        window.fbAsyncInit = function() {
            FB.init({ 
                appId: '1415201235400147', 
                channelUrl: 'partials/channel.html', 
                status: true, 
                cookie: true, 
                xfbml: true,
                version: 'v2.3'
            });

            FB.Event.subscribe('auth.statusChange', function(response) {
                $rootScope.$broadcast("fb_statusChange", {'status': response.status});
            });
        };

        (function (d) {
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement('script');
            js.id = id;
            js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        }(document));

        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            
            userObj = {};

            Data.get('session').then(function (results) {
                if (results.uid) {
                    $rootScope.userObj = results;
                    
                    $rootScope.userObj.FB_Conn = $rootScope.userObj.FB_Conn * 1;

                    $rootScope.userObj.authenticated = true;
                } else {
                    var nextUrl = next.$$route.originalPath;
                    if (nextUrl == '/signup' || nextUrl == '/login') {

                    } else {
                        $location.path("/login");
                    }
                }
            });
        });
    });
