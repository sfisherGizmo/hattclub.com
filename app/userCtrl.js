app.controller( 'UserController', function ($scope, $rootScope, $routeParams, $location, $http, Data, $window) {
    
    $scope.uid = $routeParams.uid;
    $scope.route = 'Profile';
    $scope.fb = {};

    $scope.$on( '$routeChangeSuccess', function( uid ) {
        Data.get( '/user/'+$scope.uid ).then( function( results ) {
            $rootScope.currentUser = results.user;
            console.log("UserResults",results.user);
        });
    });

    $scope.saveUser = function(user) {
        Data.post('/user/'+$scope.uid+'/update', {
            user: user
        }).then(function (results) {
            Data.toast(results);
            if (results.status == "success") {
                console.log("Congrats");
            }
        }); 
    }

    $scope.fbConnect = function() {
        // is FB loaded?
        if( typeof(FB) != 'undefined' && FB != null ) {
            FB.getLoginStatus(function (response) {
                //console.log(response);
                if (response.status === 'connected') {
                    $scope.fb.user = true;
                    $rootScope.currentUser.FB_Conn = true;
                    $rootScope.currentUser.FB_Id = response.authResponse.userID;
                    $rootScope.currentUser.Token = response.authResponse.accessToken;
                    Data.facebook('/user/', $rootScope.currentUser);
                }
            });
            if (!$scope.fb.user) {
                FB.login(function (response) {
                    //console.log(response);
                    if (response.authResponse) {
                        $rootScope.currentUser.FB_Conn = true;
                        $rootScope.currentUser.FB_Id = response.authResponse.userID;
                        $rootScope.currentUser.Token = response.authResponse.accessToken;
                        Data.facebook('/user/',$rootScope.currentUser);
                    }else { 
                        console.log('User cancelled login or did not fully authorize.');
                    }
                });
            }
            Data.post('/user/'+$scope.uid+'/update', {
                user: $rootScope.currentUser
            });
        } else {
            console.log("nope");
        }
    }
});