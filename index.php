<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en" ng-app="hattClub">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title>Hatt Club: Experience the Rewards of Giving</title>
        <!-- Bootstrap -->
        <link rel="shortcut icon" href="https://s3-us-west-2.amazonaws.com/hattimages/home/hc_fb_icon.png">
        <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Cantarell:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lobster+Two:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Kelly+Slab' rel='stylesheet' type='text/css'>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
        <link href="css/toaster.css" rel="stylesheet">
        <!-- <link rel="stylesheet" type="text/css" href="http://d2pzqhrjh94sd5.cloudfront.net/assets/css/font-awesome-4.3.0/css/font-awesome.min.css"> -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <style>
          a {
          color: orange;
          }
        </style>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]><link href= "css/bootstrap-theme.css"rel= "stylesheet" >
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body ng-cloak="">
        <header class="navbar navbar-inverse navbar-fixed-top" role="navigation" ng-include="'partials/nav.html'"></header>
        <div id="fb-root"></div>
        <div class="main-content">
            <div class="container" style="margin-top:70px; margin-bottom: 155px;">
                <div class="breadcrumbs" id="breadcrumbs">
                    <ul class="breadcrumb site-nav">
                        <li>
                            <a ng-if="userObj.uid!==''" href="#/dashboard" title="Home">
                                <i class="ace-icon fa fa-home home-icon"></i>
                            </a>
                            <a ng-if="userObj.uid==''" href="#/dashboard" title="Home">
                                <i class="ace-icon fa fa-home home-icon"></i>
                            </a>
                            <!-- <a ng-switch-when="" ng-href="#/"></a>
                            <a ng-switch-default ng-href="#/dashboard">Home</a> -->
                        </li>
                        <li class="active">{{route}}</li>
                    </ul>
                    <ul class="navbar-header site-nav">
                        <li ng-show="userObj.uid"><a class="" rel="" title="Profile" ng-href="#/user/{{ userObj.uid }}">Profile</a></li>
                        <li ng-show="userObj.uid"><a rel="" title="Team" ng-href="#/user/:uid/team">Teams</a></li>
                        <!-- <li><a><i class="fa fa-caret-down"></i></a></li> -->
                    </ul>
                    <ul class="navbar-header">
                        <li>
                            <button type="submit" class="width-35 pull-right btn btn-sm btn-primary" ng-click="logOut()">
                                Logout 
                            </button>
                        </li>
                    </ul>
                    <div style="clear:both;"></div>
                </div>
                <div data-ng-view="" id="ng-view" class="slide-animation"></div>
            </div>
            <footer class="foot-bar" ng-include="'partials/footer.html'" id="footer"></footer>
        </div>
    </body>
    <toaster-container toaster-options="{'time-out': 3000}"></toaster-container>
    <!-- Libs -->
    <script src="js/angular.min.js"></script>
    <script src="js/angular-route.min.js"></script>
    <script src="js/angular-animate.min.js" ></script>
    <script src="js/toaster.js"></script>
    <script src="app/app.js"></script>
    <script src="app/data.js"></script>
    <script src="app/directives.js"></script>
    <script src="app/authCtrl.js"></script>
    <script src="app/userCtrl.js"></script>
</html>

