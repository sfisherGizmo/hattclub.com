<?php 
$app->get('/session', function() {
    $db = new DbHandler();
    $session = $db->getSession();
    // $response["uid"] = $session['uid'];
    // $response["email"] = $session['email'];
    // $response["name"] = $session['name'];
    // $response["FB_Conn"] = $session['FB_Conn'];
    // $response["FB_Id"] = $session['FB_Id'];
    // $response["created"] = $session['created'];
    echoResponse(200, $session);
});

$app->post('/login', function() use ($app) {
    require_once 'passwordHash.php';
    $r = json_decode($app->request->getBody());
    verifyRequiredParams(array('email', 'password'),$r->customer);
    $response = array();
    $db = new DbHandler();
    $password = $r->customer->password;
    $email = $r->customer->email;
    $query = "SELECT ID, F_Name, L_Name, User_Name, Pass, User_Date, FB_Conn, FB_Id
            FROM users 
            WHERE User_Name = :user_name";
    $params = array(
        ':user_name' => $email,
    );
    $user = $db->getOneRecord($query, $email, $params);
    if ($user != NULL) {
        if(passwordHash::check_password($user[0]->Pass,$password)){
            $response['status'] = "success";
            $response['message'] = 'Logged in successfully.';
            $response['name'] = $user[0]->F_Name . " " . $user[0]->L_Name;
            $response['uid'] = $user[0]->ID;
            $response['email'] = $user[0]->User_Name;
            $response['FB_Conn'] = $user[0]->FB_Conn;
            $response['FB_Id'] = $user[0]->FB_Id;
            $response['createdAt'] = $user[0]->User_Date;
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['uid'] = $user[0]->ID;
        $_SESSION['email'] = $email;
        $_SESSION['name'] = $response['name'];
        $_SESSION['FB_Conn'] = $response['FB_Conn'];
        $_SESSION['FB_Id'] = $response['FB_Id'];
        $_SESSION['created'] = $response['createdAt'];
        } else {
            $response['status'] = "error";
            $response['message'] = 'Login failed. Incorrect credentials';
        }
    }else {
            $response['status'] = "error";
            $response['message'] = 'No such user is registered';
        }
    echoResponse(200, $response);
});
$app->post('/signUp', function() use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    verifyRequiredParams(array('User_Name', 'F_Name', 'L_Name', 'Pass'),$r->customer);
    require_once 'passwordHash.php';
    $db = new DbHandler();
    $fname = $r->customer->F_Name;
    $lname = $r->customer->L_Name;
    $email = $r->customer->User_Name;
    $password = $r->customer->Pass;
    $query = "SELECT ID, F_Name, L_Name, Pass, User_Name, User_Date 
            FROM users 
            WHERE User_Name = :user_name";
    $params = array(
        ':user_name' => $email,
    );
    $isUserExists = $db->getOneRecord($query, $email, $params);
    if(!$isUserExists){
        $r->customer->Pass = passwordHash::hash($password);
        $table_name = "users";
        $column_names = array( 'User_Name', 'F_Name', 'L_Name', 'Pass' );
        $result = $db->insertIntoTable($r->customer, $column_names, $table_name);
        if ($result != NULL) {
            $response["status"] = "success";
            $response["message"] = "User account created successfully";
            $response["uid"] = $result;
            if (!isset($_SESSION)) {
                session_start();
            }
            $_SESSION['uid'] = $response["uid"];
            $_SESSION['name'] = $fname . " ". $lname;
            $_SESSION['email'] = $email;
            $_SESSION['FB_Conn'] = false;
            $_SESSION['FB_Id'] = null;
            echoResponse(200, $response);
        } else {
            $response["status"] = "error";
            $response["message"] = "Failed to create customer. Please try again";
            echoResponse(400, $response);
        }            
    }else{
        $response["status"] = "error";
        $response["message"] = "A user with the provided phone or email exists!";
        echoResponse(400, $response);
    }
});
$app->get('/logout', function() {
    $db = new DbHandler();
    $session = $db->destroySession();
    $response["status"] = "info";
    $response["message"] = "Logged out successfully";
    echoResponse(200, $response);
});
?>