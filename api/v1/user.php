<?php

$app->get( '/user/:uid', function( $uid ) use ( $app ) {
	$db = new DbHandler();
	$query = "SELECT ID, F_Name, L_Name, User_Name, User_Date, FB_Conn, FB_Id, City, State, Zip FROM users WHERE ID = :ID";
	$params = array(
		":ID" => (int) $uid,
	);
	$user = $db->getOneRecord( $query, $uid, $params );
	$user[0]->FB_Conn = (bool) $user[0]->FB_Conn;
	$user[0]->name = $user[0]->F_Name . " " . $user[0]->L_Name;
	$user[0]->status = "success";
	echoResponse( 200, array( "user" => $user[0] ) );
});

$app->post( '/user/:uid/update', function( $uid ) use ( $app ) {
	
	$r = json_decode($app->request->getBody());
	verifyRequiredParams( array( 'User_Name', 'name' ), $r->user );
	$user = $r->user;
	$split_name = explode(" ", $user->name);
	$db = new DbHandler();
	$query = "UPDATE `users` SET `User_Name` = :User_Name, `F_Name` = :F_Name, `L_Name` = :L_Name, `City` = :City, `State` = :State, `Zip` = :Zip, `FB_Conn` = :FB_Conn, `FB_Id` = :FB_Id WHERE  `ID` = :ID";
	
	$params = array( 
		":ID" 		 => $uid,
		":User_Name" => $user->User_Name,
		":F_Name" 	 => $split_name[0],
		":L_Name" 	 => $split_name[1],
		":City" 	 => $user->City,
		":State" 	 => $user->State,
		":Zip" 		 => $user->Zip,
		":FB_Conn" 	 => $user->FB_Conn,
		":FB_Id" 	 => $user->FB_Id,
	); 
	$update = $db->updateRecord($query, $params);
	if (!$update) {
		$response["status"] = "error";
        $response["message"] = "There was an error. Please try your request again.";
       	echoResponse(400, $response);
       	return;
	}
	$response["status"] = "success";
    $response["message"] = "Your profile was updated.";
	echoResponse(200, $response);
});

?>