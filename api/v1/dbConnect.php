<?php

class dbConnect {

    private $conn;

    function __construct() {        
    }

    /**
     * Establishing database connection
     * @return database connection handler
     */
    function connect() {
        include_once '../config.php';

        // Connecting to mysql database
        $this->conn = new PDO('mysql:host='.DB_HOST.';dbname='. DB_NAME, DB_USERNAME, DB_PASSWORD);
        // Check for database connection error
        if ($this->conn->errorCode()) {
            echo "Failed to connect to MySQL: " . $this->conn->errorInfo();
        }

        // returing connection resource
        return $this->conn;
    }

}

?>
