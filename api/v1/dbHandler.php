<?php

class DbHandler {

    private $conn;

    function __construct() {
        require_once 'dbConnect.php';
        // opening db connection
        $db = new dbConnect();
        $this->conn = $db->connect();
    }

    /**
     * Fetching single record
     */
    public function getOneRecord($query, $user_name, $params) {

        $stmt = $this->conn->prepare($query);
        $stmt->execute($params);
        return $result = $stmt->fetchAll(PDO::FETCH_CLASS);    
    }

    /**
     * Creating new record
     */
    public function insertIntoTable($obj, $column_names, $table_name) {
        
        $c = (array) $obj;
        $keys = array_keys($c);
        $columns = '';
        $params = '';
        $values = '';
        $stmt = array();
        foreach($column_names as $desired_key){ // Check the obj received. If blank insert blank into the array.
           if(!in_array($desired_key, $keys)) {
                $$desired_key = '';
            }else{
                $$desired_key = $c[$desired_key];
            }
            $stmt[':'.$desired_key] = $$desired_key;
            $params = $params." :".$desired_key.',';
            $columns = $columns." ".$desired_key.',';
            //$values = $values." ".$$desired_key.",";
        }
        $query = "INSERT INTO ".$table_name." (".trim($columns,',')." ) VALUES (".trim($params,',')." )";
        $r = $this->conn->prepare($query);
        $r->execute($stmt);
        if ($r) {
            $new_row_id = $this->conn->lastInsertId();
            return $new_row_id;
            } else {
            return NULL;
        }
    }

    /**
     * 
     *
     */
    public function updateRecord($query, $params) {
        $stmt = $this->conn->prepare($query);
        if( false === ( $sql = $stmt->execute($params) ) ) {
            $arr = $stmt->errorInfo();
            return $arr;
        }

        return $sql;
    } 


    public function getSession() {

        if (!isset($_SESSION)) {
            session_start();
        }
        $sess = array();
        if(isset($_SESSION['uid']))
        {
            $sess["uid"]     = $_SESSION['uid'];
            $sess["name"]    = $_SESSION['name'];
            $sess["email"]   = $_SESSION['email'];
            $sess['FB_Conn'] = $_SESSION['FB_Conn'];
            $sess['FB_Id']   = $_SESSION['FB_Id'];
            $sess['created'] = $_SESSION['created'];
        }
        else
        {
            $sess["uid"]    = '';
            $sess["name"]   = 'Guest';
            $sess["email"]  = '';
        }
        return $sess;
    }

    public function destroySession() {

        if (!isset($_SESSION)) {
            session_start();
        }
        if(isset($_SESSION['uid']))
        {   
            unset($_SESSION['uid']);
            unset($_SESSION['name']);
            unset($_SESSION['email']);
            unset($_SESSION['FB_Conn']);
            unset($_SESSION['FB_Id']);
            unset($_SESSION['created']);
            //session_destroy();
            $info='info';
        if(isset($_COOKIE[$info]))
        {
            setcookie ($info, '', time() - $cookie_time);
        }
        $msg="Logged Out Successfully...";
        }
        else
        {
            $msg = "Not logged in...";
        }
        return $msg;
    }
}

?>
